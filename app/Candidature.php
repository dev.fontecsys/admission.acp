<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidature extends Model
{
    protected $guarded = [];
    
    public function carteCredit(){
        return $this->belongsTo('App\CarteCredit');
    }

    public function paysNaissance(){
        return $this->belongsTo('App\Pays', 'pays_naissance_id');
    }

    public function paysBac(){
        return $this->belongsTo('App\Pays', 'pays_bac_id');
    }

    public function paysAutreDiplome(){
        return $this->belongsTo('App\Pays', 'pays_autre_diplome_id');
    }

    public function emploi(){
        return $this->hasMany('App\Emploi');
    }
}
