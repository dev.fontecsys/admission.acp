<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CandidatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required|max:50|min:50',
            'prenom' => 'required|max:50',
            'date' => 'required|date',
            'ville' => 'required|max:25',
            'pays' => 'required|max:25',
            'langue-usage' => 'required',
            'langue-maternelle' => 'required|date',
            'nom-pere' => 'required|max:25',
            'prenom-pere' => 'required|max:25',
            'nom-mere' => 'required|max:25',
            'prenom-mere' => 'required|max:25',
        ];
    }
}
