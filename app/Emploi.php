<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emploi extends Model
{
    protected $guarded = [];

    public function candidature(){
        return $this->belongsTo('App\Candidature');
    }

}
