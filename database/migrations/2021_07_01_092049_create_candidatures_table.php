<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidatures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('admission_effectuee');
            $table->boolean('cours_suivi');
            $table->string('code_permanent');
            $table->string('nom');
            $table->string('prenom');
            $table->date('date_naissance');
            $table->string('sexe');
            $table->string('citoyennete');
            $table->string('ville_naissance');
            $table->unsignedBigInteger('pays_naissance_id')->nullable();
            $table->string('langue_usage');
            $table->string('langue_maternelle');
            $table->string('nom_pere');
            $table->string('prenom_pere');
            $table->string('nom_mere');

            $table->string('numero_civique');
            $table->string('rue');
            $table->string('appartement');
            $table->string('boite_postale');
            $table->string('municipalite');
            $table->string('province');
            $table->string('telephone_principal');
            $table->string('autre_telephone');
            $table->string('couriel');

            $table->enum('semestre_choisi', ['aôut/septembre', 'janvier'])->default('aôut/septembre');
            $table->string('annee');
            $table->enum('regime_etude1', ['temps complet', 'temps partiel'])->default('temps complet');
            $table->unsignedBigInteger('programme1_id');
            $table->enum('regime_etude2', ['temps complet', 'temps partiel'])->default('temps complet');
            $table->unsignedBigInteger('programme2_id')->nullable();

            $table->boolean('baccalauriat_obtenu');
            $table->string('nom_institution')->nullable();
            $table->unsignedBigInteger('pays_bac_id')->nullable();
            $table->string('annee_scolaire_bac')->nullable();

            $table->boolean('autre_diplome');
            $table->string('nom_diplome')->nullable();
            $table->string('nom_institution_autre_diplome')->nullable();
            $table->unsignedBigInteger('pays_autre_diplome_id')->nullable();
            $table->string('annee_scolaire_autre_diplome')->nullable();

            $table->boolean('aucun_diplome_obtenu')->default(false);
            $table->boolean('femme_au_foyer')->default(false);
            $table->boolean('jeune_fille')->default(false);
            $table->boolean('a_des_enfants')->default(false);
            $table->unsignedInteger('nombre_enfants')->default(0);
            $table->string('ages_enfants');

            $table->text('lettre_motivation')->nullable();
            $table->string('fichier_lettre_motivation')->nullable();

            
            $table->string('nom_proprietaire');
            $table->date('date_naissance_proprietaire');
            $table->string('sexe_proprietaire');
            $table->string('couriel_proprietaire');
            $table->enum('type_paiement', ['carte de credit', 'airtel money', 'virement bancaire', 'autre'])->default('airtel money');
            $table->unsignedBigInteger('carte_credit_id')->nullable();

            $table->timestamps();

            $table->foreign('carte_credit_id')->references('id')->on('carte_credits')->onDelete('set null');
            $table->foreign('pays_naissance_id')->references('id')->on('pays')->onDelete('set null');
            $table->foreign('pays_bac_id')->references('id')->on('pays')->onDelete('set null');
            $table->foreign('pays_autre_diplome_id')->references('id')->on('pays')->onDelete('set null');
            $table->foreign('programme1_id')->references('id')->on('programme')->onDelete('set null');
            $table->foreign('programme2_id')->references('id')->on('programme')->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::dropIfExists('candidatures');
    }
}
