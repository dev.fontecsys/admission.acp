<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmploisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emplois', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom_employeur');
            $table->string('fonction_occupee');
            $table->string('periode');
            $table->enum('type_emploi', ['rémunéré', 'stage', 'bénévolat', 'autres'])->default('rémunéré');
            $table->unsignedBigInteger('candidature_id');
            $table->timestamps();

            
            $table->foreign('candidature_id')->references('id')->on('candidature')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emplois');
    }
}
