<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarteCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carte_credits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type_carte', ['visa', 'master card', 'American Express'])->default('visa');
            $table->double('montant_verse');
            $table->string('numero_carte');
            $table->date('date_expiration');
            $table->string('code_cvc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carte_credits');
    }
}
