<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\CarteCredit::class, function (Faker $faker) {
    return [
        'type_carte' => $faker->randomElement(['visa', 'master card', 'American Express']),
        'montant_verse' => $faker->randomFloat(),
        'numero_carte' => $faker->word,
        'date_expiration' => $faker->date(),
        'code_cvc' => $faker->word,
    ];
});
