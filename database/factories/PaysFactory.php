<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Pays::class, function (Faker $faker) {
    return [
        'code' => $faker->word,
        'libelle' => $faker->word,
    ];
});
