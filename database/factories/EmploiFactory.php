<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Emploi::class, function (Faker $faker) {
    return [
        'nom_employeur' => $faker->word,
        'fonction_occupee' => $faker->word,
        'periode' => $faker->word,
        'type_emploi' => $faker->randomElement(['rémunéré', 'stage', 'bénévolat', 'autres']),
        'candidature_id' => factory(App\Candidature::class),
    ];
});
